Economics data generated or curated as part of http://openeconomics.net/ project during 2005-2008.

All this data Used to form part of http://openeconomics.net/store until July 2011. From July 2011 until November 2011 it was located at http://old.openeconomics.net/. It was finally shut down in November 2011 with all material is gradually being transferrred to http://thedatahub.org/group/open-economics.

You can see what the site used to look like using the WayBack machine at:

http://web.archive.org/web/20110109091529/http://openeconomics.net/

